@extends('layouts.app')

@section('content')
	@if(Auth::check())
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">Chat</div>

                      		<private-chat :room="{{$room}}"  :user="{{Auth::user()}}"></private-chat>
                    </div>
                </div>
            </div>
        </div>
	@endif
@endsection
