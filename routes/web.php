<?php


/*
Route::post('messages', function( Illuminate\Http\Request $request ){
	App\Events\Message::dispatch($request -> input('body'));

});
*/
Route::get('/item', 'ItemController@create');
Route::post('item', 'ItemController@store')->name('item.store');


//Route::get('/', function () {
 //   return view('chat');
//});



Route::post('messages22', function( Illuminate\Http\Request $request ){

	App\Events\PrivateChat::dispatch($request -> all());

});

Route::get('/room/{room}', function( App\Room $room ){
	return view('room' , ['room' => $room]);

});

Auth::routes();


Route::get('/home', 'HomeController@index')->name('home');

Route::resource('passports','PassportController');

